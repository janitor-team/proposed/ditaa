ditaa (0.10+ds1-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Work around the FTBFS with Java 10 by removing the JavadocTaglet class.
    (Closes: #897494)

 -- Markus Koschany <apo@debian.org>  Wed, 13 Jun 2018 23:03:10 +0200

ditaa (0.10+ds1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Apply FTBFS fix from Julien Pinon. (Closes: #802065)

 -- Adrian Bunk <bunk@debian.org>  Fri, 13 Jan 2017 19:13:14 +0200

ditaa (0.10+ds1-1) unstable; urgency=medium

  * Team upload of new upstream release.
  * debian/copyright: Add Files-Excluded field.
  * debian/watch: Update to rely on github tag and to add repack suffix.
  * debian/rules: Adjust the get-orig-source target.
  * Drop 01-disable_debug.patch, merged upstream.

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 20 Jul 2015 15:06:10 +0200

ditaa (0.9+git20150709-1) unstable; urgency=medium

  * Team upload.
  * New snapshot of the upstream git repository. Closes: #787074
  * Update Homepage field with http://ditaa.sourceforge.net. Closes: #734533
  * debian/manifest: add /usr/share/java/jericho-html.jar to classpath
    field. Closes: #748020
  * Drop patch 01-fix_jericho_API.patch, upstream now does support version 3.1
    of the Jericho API.
  * Refresh the other patches.
  * Add debian/patches/01-disable_debug.patch to disable debug mode.
  * Bump Standards-Version to 3.9.6 (no change needed).
  * Update Vcs-* fields to their latest canonical name.
  * debian/copyright:
    - follow version 1.0 of the format (reorder the blocks, replace
      Format-Specification header by Format)
    - switch to LGPL-3+ to follow upstream's move
  * Bump debhelper compatibility level to 9.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 10 Jul 2015 09:37:00 +0200

ditaa (0.9+ds1-3) unstable; urgency=low

  * debian/patches/01-fix_jericho_API.patch: re-added DEP-3 header
  * Depend on "default-jdk | java6-sdk" instead of hardcoding
    openjdk-6-jdk, this overrides the package detected by jh_depends
    (Closes: #606266)
  * Bump Standards-Version to 3.9.1, no changes needed

 -- David Paleino <dapal@debian.org>  Tue, 01 Mar 2011 12:11:29 +0100

ditaa (0.9+ds1-2) unstable; urgency=low

  * debian/patches/01-fix_jericho_API.patch refreshed, use jericho
    3.1 API (Closes: #581325)
  * debian/control:
    - Build-Depends on libjerico-html-java bumped to >= 3.1
    - ease backporters' life appending "~" to debhelper's version
    - added runtime dependency on jericho

 -- David Paleino <dapal@debian.org>  Wed, 12 May 2010 23:43:42 +0200

ditaa (0.9+ds1-1) unstable; urgency=low

  * Initial release (Closes: #577020)

 -- David Paleino <dapal@debian.org>  Thu, 15 Apr 2010 23:38:02 +0200
